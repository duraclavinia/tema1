package services;

import java.time.LocalDate;

import io.grpc.stub.StreamObserver;
import proto.PersonServiceGrpc;
import proto.Person;


public class PersonImpl extends PersonServiceGrpc.PersonServiceImplBase{

   public static String ReturnGender(String CNP)
   {
       String gender = null;
       switch(CNP.charAt(0))
       {
           case '1':
           case '5':
           case '3':
           case '7':
               gender = "male";
               break;
           case '2':
           case '4':
           case '6':
           case '8':
               gender = "female";
               break;
       }
       return gender;
   }
   public static String ReturnAge(String CNP) {
       LocalDate now = LocalDate.now();
       int currentYear = now.getYear();
       StringBuilder year = new StringBuilder();
       String CNPYear = null;

       switch (CNP.charAt(0)) {
           case '3':
           case '4': {
               CNPYear = "18";
               break;
           }

           case '1':
           case '2':
           case '8':
           case '7': {
               CNPYear = "19";
               break;
           }

           case '5':
           case '6': {
               CNPYear = "20";
               break;
           }
       }
       year.append(CNPYear + CNP.substring(1,3));
       int yearToReturn = Integer.parseInt(String.valueOf(year));

       return String.valueOf(currentYear - yearToReturn);

   }
   public void getNameAndCNP(Person.UserRequest request, StreamObserver<Person.Response> responseStreamObserver)
   {
       Person.Response response = Person.Response.newBuilder().setName(request.getName()).setGender(ReturnGender(request.getCnp())).setAge(ReturnAge(request.getCnp())).build();
       System.out.println("The name is: " + request.getName() + " the gender is: " + response.getGender() + " the age is: " + response.getAge());
       responseStreamObserver.onNext(response);
       responseStreamObserver.onCompleted();
   }

}
