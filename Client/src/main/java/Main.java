import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import proto.Person;
import proto.PersonServiceGrpc;

import java.util.Scanner;

public class Main{
    public static void main(String[] args)
    {
        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 8999).usePlaintext().build();
        PersonServiceGrpc.PersonServiceBlockingStub personStub = PersonServiceGrpc.newBlockingStub(channel);

        boolean isConnected = true;
        while(isConnected)
        {
            Scanner scan = new Scanner(System.in);

            System.out.println("Please insert your name: ");
            String name = scan.next();

            System.out.println("Insert your CNP: ");
            String CNP = scan.next();

            Person.Response data;
            data = personStub.getNameAndCNP(Person.UserRequest.newBuilder().setName(name).setCnp(CNP).build());
            System.out.println("Data added!");

            System.out.println(data); //pt a afisa raspunsul primit de client

        }
        channel.shutdown();
    }
}